import timeit
from main import data

def bubble_sort(data):
    swapped = True
    while swapped:
        swapped = False
        for _ in range(len(data)-1):
            if data[_] > data[_ + 1]:
                data[_], data[_ + 1] = data[_ + 1], data[_]
                swapped = True
    return data


def insertion_sort(data):
    for i in range(1, len(data)):
        item_to_insert = data[i]
        j = i - 1
        while j >= 0 and data[j] > item_to_insert:
            data[j + 1] = data[j]
            j -= 1
        data[j + 1] = item_to_insert
    return data


def shell_sort(data):
    last_index = len(data)
    step = len(data) // 2
    while step > 0:
        for i in range(step, last_index, 1):
            j = i
            delta = j - step
            while delta >= 0 and data[delta] > data[j]:
                data[delta], data[j] = data[j], data[delta]
                j = delta
                delta = j - step
        step //= 2
    return data


def quicksort(data):
    if len(data) <= 1:
        return data

    pivot = data[0]
    smaller = list(filter(lambda x: x < pivot, data))
    equal = list(filter(lambda x: x == pivot, data))
    greater = list(filter(lambda x: x > pivot, data))

    return quicksort(smaller) + equal + quicksort(greater)

data_bubble = data.copy()
data_insertion = data.copy()
data_shell = data.copy()
data_quick = data.copy()

print('---ПУЗЫРЬКОВАЯ СОРТИРОВКА---')
start_time = timeit.default_timer()
bubble_sort(data_bubble)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---СОРТИРОВКА ВСТАВКАМИ---')
start_time = timeit.default_timer()
insertion_sort(data_insertion)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---СОРТИРОВКА ШЕЛЛА---')
start_time = timeit.default_timer()
shell_sort(data_shell)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)

print('---БЫСТРАЯ СОРТИРОВКА---')
start_time = timeit.default_timer()
quicksort(data_quick)
stop_time = timeit.default_timer()
print('Время работы', stop_time - start_time)
