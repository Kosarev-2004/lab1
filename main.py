import random

data = [random.uniform(0.0, 100.0) * n for n in range(50)]

def bubble_sort(data):
    swapped = True
    while swapped:
        swapped = False
        for _ in range(len(data)-1):
            if data[_] > data[_ + 1]:
                data[_], data[_ + 1] = data[_ + 1], data[_]
                swapped = True
    return data
print("----------ПУЗЫРЬКОВАЯ СОРТИРОВКА------------")

print("До: ", data)
bubble_sort(data)
print("После", data)

data_1 = [random.uniform(0.0, 100.0) * n for n in range(50)]
def insertion_sort(data_1):
    for i in range(1, len(data_1)):
        item_to_insert = data_1[i]
        j = i - 1
        while j >= 0 and data_1[j] > item_to_insert:
            data_1[j + 1] = data_1[j]
            j -= 1
        data_1[j + 1] = item_to_insert
    return data_1
print("\n----------СОРТИРОВКА ВСТАВКАМИ------------")

print("До: ", data_1)
insertion_sort(data_1)
print("После", data_1)

data_2 = [random.uniform(0.0, 100.0) * n for n in range(50)]
def shell_sort(data_2):
    last_index = len(data_2)
    step = len(data_2) // 2
    while step > 0:
        for i in range(step, last_index, 1):
            j = i
            delta = j - step
            while delta >= 0 and data_2[delta] > data_2[j]:
                data_2[delta], data_2[j] = data_2[j], data_2[delta]
                j = delta
                delta = j - step
        step //= 2
    return data_2
print("\n----------СОРТИРОВКА ШЕЛЛА------------")

print("До: ", data_2)
print("После", shell_sort(data_2))

data_3 = [random.uniform(0.0, 100.0) * n for n in range(50)]
def quicksort(data_3):
    if len(data_3) <= 1:
        return data_3

    pivot = data_3[0]
    smaller = list(filter(lambda x: x < pivot, data_3))
    equal = list(filter(lambda x: x == pivot, data_3))
    greater = list(filter(lambda x: x > pivot, data_3))

    return quicksort(smaller) + equal + quicksort(greater)
print("\n----------БЫСТРАЯ СОРТИРОВКА------------")
print("До: ", data_3)

print("После", quicksort(data_3))
