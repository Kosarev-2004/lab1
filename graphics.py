import matplotlib.pyplot as plt
import random
import timeit
import numpy as np
from main import bubble_sort, insertion_sort, shell_sort, quicksort


def create_plot(x: list, y: list, title: str):
    fig, ax = plt.subplots(1, 1)
    z = np.polyfit(x, y, 1)
    p = np.poly1d(z)
    ax.plot(x, y)
    ax.set_xlabel('Количество элементов')
    ax.set_ylabel('Время вычисления, с')
    fig.text(0.4, 0.9, title)
    fig.show()


# Проверка сложности
print('Начинаем проверку сложности')
t_bubble = []
t_insert = []
t_shell = []
t_quick = []

amount = int(input('Введите количество сотен элементов - '))

data = []
parameters = []
for _ in range(amount):
    for i in range(100):
        data.append(random.randint(0, 100))
    print('Круг - ', _)
    parameters.append((_+1)*100)
    start_time = timeit.default_timer()
    insertion_sort(data)
    end_time = timeit.default_timer()
    t_insert.append(end_time - start_time)

data = []
parameters = []

for _ in range(amount):
    for i in range(100):
        data.append(random.randint(0, 100))
    print('Круг - ', _)
    parameters.append((_ + 1) * 100)
    start_time = timeit.default_timer()
    shell_sort(data)
    end_time = timeit.default_timer()
    t_shell.append(end_time - start_time)

data = []
parameters = []

for _ in range(amount):
    for i in range(100):
        data.append(random.randint(0, 100))
    print('Круг - ', _)
    parameters.append((_ + 1) * 100)
    start_time = timeit.default_timer()
    quicksort(data)
    end_time = timeit.default_timer()
    t_quick.append(end_time - start_time)

data = []
parameters = []

for _ in range(amount):
    for i in range(100):
        data.append(random.randint(0, 100))
    print('Круг - ', _)
    parameters.append((_ + 1) * 100)
    start_time = timeit.default_timer()
    bubble_sort(data)
    end_time = timeit.default_timer()
    t_bubble.append(end_time - start_time)

create_plot(parameters, t_bubble, 'Сортировка пузырьком')
plt.show()
create_plot(parameters, t_insert, 'Сортировка вставками')
plt.show()
create_plot(parameters, t_shell, 'Сортировка Шелла')
plt.show()
create_plot(parameters, t_quick, 'Быстрая сортировка')
plt.show()
